import os
from distutils.core import setup


def read_file_into_string(filename):
    path = os.path.abspath(os.path.dirname(__file__))
    filepath = os.path.join(path, filename)
    try:
        return open(filepath).read()
    except IOError:
        return ''


def get_readme():
    for name in ('README', 'README.rst', 'README.md'):
        if os.path.exists(name):
            return read_file_into_string(name)
    return ''


setup(
    name='flickrapi_toolkit',
    packages=[
            'flickrapi_toolkit',
            'flickrapi_toolkit.management.commands',
            'flickrapi_toolkit.migrations',
            'flickrapi_toolkit.tests',
            'flickrapi_toolkit.tests.scenarios',
            'example_flickrapi_toolkit',
            'example_flickrapi_toolkit.templatetags',
            ],
    package_data={
        'flickrapi_toolkit': [
            'static/*.*',
            'static/flickrapi_toolkit/*.*',
            'static/flickrapi_toolkit/js/*.*',
        ],
        'example_flickrapi_toolkit': [
            'templates/*.*',
            'templates/dash/*.*',
            'templates/example/*.*',
        ],
    },
    version='0.0.1',
    description='FlickrAPI Toolkit',
    author='Tim Bushell',
    author_email='tcbushell@gmail.com',
    url='git@github.com:tcbushell/flickrapi_toolkit.git',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Framework :: Django :: 1.10',
        'Topic :: Development :: API',
    ],
    long_description=get_readme(),
)
