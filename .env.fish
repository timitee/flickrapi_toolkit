source venv-flickrapi_toolkit/bin/activate.fish
set -x DJANGO_SETTINGS_MODULE "example_flickrapi_toolkit.dev_test"
set -e FLICKR_UID
set -e FLICKR_KEY
set -e FLICKR_SECRET
set -e PHOTOS_ROOT_FOLDER
source .private
