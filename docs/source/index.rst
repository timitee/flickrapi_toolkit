.. FkickrAPI Toolkit documentation master file, created by
   sphinx-quickstart on Sat Mar 18 19:30:52 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FkickrAPI Toolkit's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   marshall
   toolkit
   flickr_sync
   decorators
   service
   models



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
