![](./timicard.png)

# flickrapi_toolkit

The Python FlickrAPI (flickrapi) is a robust and workable wrapper
for Flickr's API.
    https://github.com/sybrenstuvel/flickrapi

`FlickrAPIMarshall` simply marshalls common calls through to flickrapi,
which takes care of the rest.

The main purpose of `FlickrAPIToolKit` is to simplify handling
Flickr API's (https://www.flickr.com/services/api/) returned data. You
can use this class to more easily traverse the tree structure of your
Flickr account.

The FlickrAPIToolKit has it's own internal `FlickrAPIMarshall` to marshall
flickrapi's calls.

## Virtual Environment

```
virtualenv --python=python3 venv-flickrapi_toolkit
source venv-flickrapi_toolkit/bin/activate

pip install -r requirements/local.txt
pip-review --local --interactive
```

## Testing

```
find . -name '*.pyc' -delete
py.test -x
```

## Usage

```
./init_dev.sh
```


![](./appicon.png)