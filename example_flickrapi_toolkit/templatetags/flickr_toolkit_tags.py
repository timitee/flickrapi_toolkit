# -*- encoding: utf-8 -*-
from django import template

register = template.Library()

# @register.inclusion_tag('bootstrap/_cms_inctag_pagination_button.html')
# def cms_pagination_button(cur_page_no, target_page_jump, page_max):

@register.filter(name="flickr_photo_url")
def flickr_photo_url(foto_obj, size='s'):
    return 'https://farm{farm}.staticflickr.com/{server}/{foto}_{secret}_{size}.jpg'.format(
                                    farm=foto_obj['farm'],
                                    server=foto_obj['server'],
                                    foto=foto_obj['id'],
                                    secret=foto_obj['secret'],
                                    ext=foto_obj['originalformat'],
                                    size=size,
                                    )

@register.filter(name="flickr_content")
def flickr_content(flickr_obj):
    if isinstance(flickr_obj, dict):
        return flickr_obj.get('_content', '')
    return flickr_obj
