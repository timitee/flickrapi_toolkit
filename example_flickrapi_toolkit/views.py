# -*- encoding: utf-8 -*-
from __future__ import unicode_literals


from django.views.generic import TemplateView
from flickrapi_toolkit.marshall import FlickrAPIMarshall, FlickrAPIStandardAuth
from flickrapi_toolkit.models import Follection
from flickrapi_toolkit.toolkit import (
    FlickrAPIToolKit,
    follection_finder,
    fotoset_finder,
)

futh = FlickrAPIStandardAuth()
fool = FlickrAPIToolKit(futh, True)

class HomeView(TemplateView):

    template_name = 'example/home.html'

    def get_follection_id(self, root_id):
        follection_id = self.kwargs.get('follection_id', root_id)
        return follection_id


    def get_fotoset_id(self):
        fotoset_id = self.kwargs.get('fotoset_id', None)
        return fotoset_id


    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)

        # You need a root id - either hard code it or sync photos. You can
        # use our test collection:
        #    django-admin.py animals26
        root = Follection.objects.root_follection()
        root_id = root.id

        # Get all Follections and Fotosets down from the root!
        all_fotosets = fool.fotosets_of_follection(root_id, True)
        all_follections = fool.follections_of_follection(root_id, True)

        # Defaults: main_obj will be a Collection or Photoset
        main_obj = None
        fotos = []
        fotosets = []


        # Get main_obj!
        follection_id = self.get_follection_id(root_id)
        # Getting the object out of all_follections retrieves other details
        # like it's breadcrumbs and parent
        main_obj = follection_finder(follection_id, all_follections)
        if not main_obj:
            # Use the root. We won't need the breadcrumbs
            main_obj = fool.mrs_f.follection_info(root_id)
            main_obj['collection'] = [
                                        f
                                        for f in all_follections
                                        if f['parent']['id'] == follection_id]


        fotoset_id = self.get_fotoset_id()
        if fotoset_id:
            # Change the main object to a Photoset, getting it out of
            # all_fotosets so that we retreve the other details
            main_obj = fotoset_finder(fotoset_id, all_fotosets)
            # Get its photots
            fotos = fool.mrs_f.fotos_of_fotoset(fotoset_id)
        else:
            # So the main object is a Collection - get its direct photosets,
            # if any.
            fotosets = fool.fotosets_of_follection(follection_id, False)


        context.update(dict(
            main_obj=main_obj,
            fotosets=fotosets,
            fotos=fotos,
            ))
        return context
