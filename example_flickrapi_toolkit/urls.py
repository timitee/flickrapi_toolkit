# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import (
    HomeView,
)

admin.autodiscover()

urlpatterns = [
    url(regex=r'^follection/(?P<follection_id>.*)/fotoset/(?P<fotoset_id>.*)/$',
        view=HomeView.as_view(),
        name='project.fotoset.home'
        ),
    url(regex=r'^follection/(?P<follection_id>.*)/$',
        view=HomeView.as_view(),
        name='project.follection.home'
        ),
    url(regex=r'^$',
        view=HomeView.as_view(),
        name='project.home'
        ),
]



urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
