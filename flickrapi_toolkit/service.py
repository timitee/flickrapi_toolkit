# -*- encoding: utf-8 -*-
import datetime
import hashlib
import os
from operator import itemgetter
from PIL.ExifTags import TAGS
from PIL import Image



class FlickrAPIToolKitError(Exception):
    def __init__(self, called, message):
        Exception.__init__(self)
        self.notes = notes
        self.message = message

    def __str__(self):
        return repr('{}: {}'.format(self.notes, self.message))


class FlickrAPISorter(object):
    """Used to sort Flickr objects"""
    def __init__(self):
        self.sorter = []

    def count(self):
        return len(self.sorter)

    def add(self, sort_val, id):
        rtn = {}
        rtn['sort'] = sort_val
        rtn['id'] = str(id)
        self.sorter.append(rtn)

    def sorted_ids(self, reverse=False):
        sorter_ids = self.sorted(reverse)
        ids = [obj['id'] for obj in sorter_ids]
        return ','.join(map(str, ids))

    def sorted(self, reverse=False):
        return sorted(self.sorter, key=itemgetter('sort'), reverse=reverse)



def truncate(json):
    """Sometimes you only want to see the top keys of a dictionary"""
    for k, v in json.items():
        if isinstance(v, dict) or  isinstance(v, list):
            json[k] = 'truncated'
    return json


def filehash(filepath, blocksize=4096):
    """Return the hash hexdigest for the file `filepath`, processing the file
    by chunk of `blocksize`.

    :type filepath: str
    :param filepath: Path to file

    :type blocksize: int
    :param blocksize: Size of the chunk when processing the file
    """

    sha = hashlib.sha256()
    with open(filepath, 'rb') as fp:
        while 1:
            data = fp.read(blocksize)
            if data:
                sha.update(data)
            else:
                break
    return sha.hexdigest()


def foto_str_to_date(date_str, date_format='%Y-%m-%d %H:%M:%S'):
    """Convert a Flickr API `date_str` to a python datetime object"""
    return datetime.datetime.strptime(date_str, date_format)


# HIPSTAMATIC example
# {271: 'Hipstamatic',
#  272: '333',
#  274: 1,
#  282: (72, 1),
#  283: (72, 1),
#  296: 2,
#  305: 'Scott S Lens and Blanko Freedom13 Film (post-processed)',
#  306: '2017:01:07 12:49:25',
#  315: 'Tim Bushell',
#  33432: 'Tim Bushell',
#  34665: 274,
#  36864: b'0221',
#  36867: '2017:01:07 12:49:25',
#  36868: '2017:03:03 23:00:12',
#  37121: b'\x01\x02\x03\x00',
#  40960: b'0100',
#  40961: 1,
#  40962: 640,
#  40963: 640,
#  41990: 0}

def get_exif_fields(full_path, fields_list):
    """Return a list of EXIF values matching any fields in your `fields_list`"""

    values = {}
    try:
        img = Image.open(full_path)
        try:
            exif = img._getexif()
            if exif:
                for k, v in exif.items():
                    print(k, v)
                    for field in fields_list:
                        if TAGS.get(k, None) == field:
                            values[field] = v
        except Exception as ex:
            pass
        img.close()
    except:
        pass
    return values


def get_exif_field(full_path, field_no):
    """Return a list of EXIF values matching any fields in your `fields_list`"""
    try:
        with Image.open(full_path) as img:
            exif = img._getexif()
            return exif[305]
    except:
        pass
    return None



DATETAKEN_FIELDS = ['DateTimeDigitized', 'DateTimeOriginal', 'DateTime']

def get_exif_datetaken(root, photo_path):
    """Uses a list of EXIF date values against a preloaded list of common
    `DATETAKEN_FIELDS`.
    """
    date_now = datetime.datetime.now().date()
    foto_file = os.path.join(root, photo_path)
    exif_fields = get_exif_fields(foto_file, DATETAKEN_FIELDS)
    return list(exif_fields.items())[0][0] if exif_fields else date_now



def get_exif_camera_name(root, photo_path):
    """ x """
    full_path = os.path.join(root, photo_path)
    return get_exif_field(full_path, 305)
