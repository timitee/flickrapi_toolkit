# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from flickrapi_toolkit.tests.scenario import init_upload_scenario


class Command(BaseCommand):
    help='Ask for as much help as you need!'


    def handle(self, *args, **options):

        init_upload_scenario('abc')
        pass
