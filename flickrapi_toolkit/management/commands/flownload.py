# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from flickrapi_toolkit.marshall import (
    FlickrAPIStandardAuth,
    FlickrAPINoAuthAuth,
)
from flickrapi_toolkit.flickr_sync import (
    SYNC_DOWNLOADING_IMAGES,
    FlickrSync,
)


class Command(BaseCommand):
    help='Ask for as much help as you need!'

    def add_arguments(self, parser):
        """Add argument for follection."""
        parser.add_argument('--foll', nargs='+')

    def handle(self, *args, **options):

        futh = FlickrAPIStandardAuth()

        col_id = ''

        if options.get('foll', None):
            col_id = [col_id for col_id in options['foll']][-1]

        fync = FlickrSync(
            futh,
            root_foll_id=col_id,
        )
        fync.sync(
            direction=SYNC_DOWNLOADING_IMAGES,
        )
