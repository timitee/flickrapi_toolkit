# -*- encoding: utf-8 -*-
"""`FlickrSync` is for syncing a Flickr account with any photos you have located
in your file system. It is expected and encouraged you organise your photos in
directories. FlickrSync will replicate the directory structure into Flickr
Collections and Photosets.

IMPORTANT:

Flickr has a very particular way of organising your photos.

A directory on your PC which contains photos is always treated as a Photoset.

A Collection is a special folder in Flickr which can only hold Sub Collections
or Photosets, but not both.

If you have a directory containing two sub-directories and - one contains photos -
the other contains sub directories

this is illegal in Flickr because the former must be a Photoset, the latter a
Collection. Flickr can't support this.

Don't worry: `FlickrSync` will move the images from the "photo-containing"
directory down into a sub directory. In short: FlickrSync corrects your
directory structure on the fly. See: `FlickrSync.children_are_follections`.


***Usage:***

::

    from flickrapi_toolkit.marshall import FlickrAPIStandardAuth, FlickrAPINoAuthAuth
    from flickrapi_toolkit.flickr_sync import FlickrSync
    futh = FlickrAPINoAuthAuth()
    fync = FlickrSync(
                futh,
                '',  # '58879823-72157677238787383',
                )
    fync.sync()


"""

import logging

LOG_FILENAME = 'flickrapi_sync.log'
logging.basicConfig(filename=LOG_FILENAME,
                    level=logging.DEBUG,
                    )

import datetime
import os
import sys
import time
import urllib
from django.conf import settings
from tqdm import tqdm

from flickrapi_toolkit.toolkit import FlickrAPIToolKit
from flickrapi_toolkit.models import (
    Follection,
    Fotoset,
    Foto,
)
from flickrapi_toolkit.service import (
    filehash,
    get_exif_datetaken,
)

PHOTO_EXTS = ['.jpg', '.png', '.gif', '.bmp']


SYNC_READONLY = 0
SYNC_DOWNLOADING_FOLDERS = 1
SYNC_DOWNLOADING_IMAGES = 2
SYNC_UPLOADING_IMAGES = 3


class FlickrSync(FlickrAPIToolKit):
    """`FlickrSync` is for syncing a Flickr account with any photos you have
    located in the file system."""


    def __init__(self, futh, root_foll_id=None, reading_only=False):
        """
        :param futh: An instance of the `FlickrAPIStandardAuth` or
        `FlickrAPINoAuthAuth` class.

        :param root_foll_id: The Flickr Collection ID to where all your
        follections, fotosets and fotos will be synced. If you choose None
        then `FlickrSync` will create a new root  Collection in your Flickr
        account for the sync.
        """

        super().__init__(futh, reading_only)
        self.root_foll_dir = os.path.normpath(settings.PHOTOS_ROOT_FOLDER)
        self.root_foll_id = root_foll_id


    def make_path(self, obj):
        make_path = os.path.join(self.root_foll_dir, obj.path if obj.path else '')
        if not os.path.exists(make_path):
            os.makedirs(make_path)



    def sync(self, direction=SYNC_READONLY):
        """Syncs the directory structure of your photo library into
        a tree of Flickr Collections and Photosets, then uploads all your
        fotos.
        """

        if (self.root_foll_id and
            direction > SYNC_READONLY and not
            direction == SYNC_UPLOADING_IMAGES):
            """Download a snapshot of your Flickr Account."""

            print('------ DOWNLOADING ----------------------------------')

            follections = self.follections_of_follection(self.root_foll_id, True)

            for foll_json in tqdm(follections, desc='Collections', unit='col'):

                parent = Follection.file_manager.capture_json(
                                                            foll_json['parent'])

                follection = Follection.file_manager.capture_json(foll_json, parent)

                self.make_path(follection)


            fotosets = self.fotosets_of_follection(self.root_foll_id, True)

            for fset_json_mini in tqdm(fotosets, desc='Photosets', unit='phst'):

                follection = Follection.file_manager.capture_json(
                                                            fset_json_mini['parent'])

                fset_json = self.mrs_f.fotoset_info(fset_json_mini['id'])
                fotoset = Fotoset.file_manager.capture_json(fset_json, follection)

                self.make_path(fotoset)

                fotos = self.mrs_f.fotos_of_fotoset(fotoset.id)


                for foto_json in fotos:  # tqdm(fotos, desc='Photos', unit='pht'):
                    foto = Foto.file_manager.capture_json(foto_json, fotoset, follection)
                    make_path = os.path.join(
                        self.root_foll_dir,
                        foto.file_name_with_id
                    )
                    if not os.path.exists(make_path):
                        if SYNC_DOWNLOADING_IMAGES:
                            urllib.request.urlretrieve(foto.urlOriginal(),  make_path)
                        else:
                                with open(make_path, 'w') as f:
                                    f.write('foto')


        if direction == SYNC_UPLOADING_IMAGES:
            """Syncs the directory structure of your photo library into a tree
            of Flickr Collections and Photosets, then uploads all your fotos. """

            root_foll = Follection.file_manager.sync(
                self,
                self.root_foll_dir,
                self.root_foll_id
                )

            print('------ UPLOADNG ----------------------------------')

            is_follection = self.is_follection(self.root_foll_dir)

            for root, subs, files in os.walk(self.root_foll_dir,
                                                            followlinks=True):
                #
                # # Start with the directories.
                # for basename in tqdm(subs, desc='Folders', unit='fol'):
                #
                #     # Full path
                #     current_fullpath = os.path.join(root, basename)
                #
                #     is_follection = self.is_follection(current_fullpath)
                #
                #     # Some subdirs have subdirs - SYNC AS COLLECTION
                #     if is_follection:
                #
                #         obj = Follection.file_manager.sync(self, current_fullpath)
                #
                #         logging.info('Synced collections {}', current_fullpath)
                #
                #     # Contains only images - SYNC AS PHOTOSET
                #     else:
                #
                #         fotos = [
                #             img for img in os.listdir(current_fullpath)
                #             if os.path.splitext(img)[1] in PHOTO_EXTS
                #         ]
                #
                #         # Assume the first file is the primary image
                #         primary = fotos[0] if fotos else None
                #
                #         # Sync with Primary Image
                #         obj = Fotoset.file_manager.sync(self, current_fullpath,
                #                                         primary)
                #
                #         logging.info('Synced Photoset {}', current_fullpath)

                # Upload the images. Respective "follections" and fotosets will be created

                primary_fotos = [
                    img for img in files
                    if (
                        os.path.splitext(img)[1] in PHOTO_EXTS
                    and
                        os.path.split(img)[0] in ['folder', 'cover', os.path.basename(root)]
                    )
                ]

                self.upload_fotos(root, primary_fotos)

                fotos = [
                    img for img in files
                    if (
                        os.path.splitext(img)[1] in PHOTO_EXTS or
                        os.path.islink(os.path.join(root, img))
                        )
                ]

                self.upload_fotos(root, fotos)

                print("uploaded", root_foll.id)

        # Completed
        return self.root_foll_id


    def upload_fotos(self, root, fotos):

        for fil in fotos:

            image_fullpath = os.path.join(root, fil)

            # Get Date Taken value of foto
            taken = get_exif_datetaken(root, fil)

            if not taken:
                taken = datetime.datetime.now().date()

            # Sync
            obj = Foto.file_manager.sync(self, image_fullpath)

            logging.info('Synced {}', image_fullpath)



    def is_follection(self, directory):
        """Returns True or False on whether the `directory` should be treated
        as follections or fotosets.
        """

        # List images in folder (if any)
        fotos = [
            img for img in os.listdir(directory)
            if os.path.splitext(img)[1] in PHOTO_EXTS
        ]

        # List subdirs in folder (if any)
        subdirs = [
            a.name for a in os.scandir(directory)
            if not a.is_file()
        ]

        if (fotos and subdirs):
            """FACT 1:
            A directory is a follection when it contains only subdirs
            (which could be fotosets or subfollections).
            """
            # log('Directories with subdirs cannot have files:')
            fotos = self.move_fotos_down(directory)

        if subdirs:
            """FACT 2:
            A follection's subdirs must all be fotosets or subfollections,
            #  they cannot be both.
            """
            has_child_follection = self.children_are_follections(directory)

        return bool(not fotos and subdirs)


    def children_are_follections(self, directory):
        """Returns True or False on whether the sub directories of the
        `directory` should be treated as follections or fotosets.
        """

        obj_name = os.path.basename(directory)

        # List child_subdirs in folder (if any)
        child_subdirs = [
            a.name for a in os.scandir(directory)
            if not a.is_file()
        ]

        # Assume all the sub folders are fotosets
        has_child_follection = False
        for child_dir in child_subdirs:
            child_subdir = os.path.join(directory, child_dir)
            has_child_follection = (
                bool([a for a in os.scandir(child_subdir) if not a.is_file()]) or
                has_child_follection
            )

        # Even if one subdir is a follection...
        if has_child_follection:
            """FACT 3:
            We must look ahead at all subdirs of a follection. If just one
            subdir is a follection (because it has subdirs)... e.g.:

            ::

                [coll]
                    |-[pset]      <--- illegal
                        |-[photo]
                        |-[photo]
                    |-[coll]      <--- because of this
                        |-[pset]
                            |-[photo]
                            |-[photo]
                        |-[pset]
                            |-[photo]
                            |-[photo]

            ... then all subdirs with fotos must be converted to follections
            by moving that subdir and images down into a subfolder. e.g.

            ::


                [coll]
                    |-[coll]
                        |-[pset]  <--- insert folder
                            |-[photo]  --> move photos "down"
                            |-[photo]  -->
                    |-[coll]
                        |-[pset]
                            |-[photo]
                            |-[photo]
                        |-[pset]
                            |-[photo]
                            |-[photo]

            """

            # ... all subdirs must be follections.
            for child_dir in child_subdirs:
                child_subdir = os.path.join(directory, child_dir)
                fotos = [
                    img for img in os.listdir(child_subdir)
                    if os.path.splitext(img)[1] in PHOTO_EXTS
                ]
                # If dir has fotos.
                if bool(fotos):
                    # log('All subdirs of', obj_name, 'must be collections.')
                    self.move_fotos_down(child_subdir)



        return has_child_follection


    def move_fotos_down(self, directory):
        """Moves all the fotos in the `directory` down into a sub directory with
        the same name as the `directory`.
        """

        # Create a sub-subdir with the same name as the
        # directory which get's synced later.
        child_subdir = os.path.join(directory, os.path.basename(directory))
        if not os.path.exists(child_subdir):
            os.mkdir(child_subdir)

            # log('   created: {}'.format(child_subdir.replace(self.root_foll_dir, '')))

        # List images in folder (if any)
        childfotos = [
            img for img in os.listdir(directory)
            if os.path.splitext(img)[1] in PHOTO_EXTS
        ]

        # Move the images
        for childfoto in childfotos:
            """FACT 4
            Even if one subdir is a follection, all of them must be. Any subdirs
            which aren't get converted from a "subdir-with-images" into a
            "subdir-containing-a-subdir-with-images".
            """
            # Build paths
            old_path = os.path.join(directory, childfoto)
            new_path = os.path.join(child_subdir, childfoto)
            # Rename
            os.rename(old_path, new_path)

            # log('   moved: {} --> {}'.format(
            #     childfoto, new_path.replace(self.root_foll_dir, '')))

        return []


    def reset(self):
        """Uses the directory structure of a local photo library to delete
        the Collections, Photosets, and Photos which were previously synced
        by `FlickrSync`.
        """

        for root, subs, files in os.walk(self.root_foll_dir):

            # Delete the images first
            fotos = [
                img for img in files
                if os.path.splitext(img)[1] in PHOTO_EXTS
            ]

            foto_ids = []
            for foto in fotos:
                image_fullpath = os.path.join(root, foto)
                obj = Foto.file_manager.model_by_path(
                    self.root_foll_dir, image_fullpath)
                foto_ids.append(obj.id)

            for foto_id in foto_ids:

                obj = Foto.file_manager.reset(
                    self,
                    '',
                    foto_id
                )

                # log('Foto.deleted: {}'.format(foto_id))

        # Delete the root follection if not preset
        if not self.root_foll_id:
            obj = Follection.file_manager.reset(
                self,
                self.root_foll_dir,
                recursive=True,
            )
            # log('Follection.root.delete: {} {}'.format(obj.id, obj.path))

        else:

            obj = Follection.file_manager.model_by_path(
                self.root_foll_dir,
                self.root_foll_dir,
            )
            for foll in Follection.objects.filter(follection=obj):
                obj = Follection.file_manager.reset(
                    self,
                    foll.path,
                    foll.id,
                    recursive=True,
                )
                # log('Follection.delete: {} {}'.format(obj.id, obj.path))
        pass
