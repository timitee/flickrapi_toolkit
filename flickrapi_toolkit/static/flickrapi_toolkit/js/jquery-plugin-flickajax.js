/********  "Havijax" plugin  ****

Abstract: Overrides any links and implement as an AJAX call instead.

Usage:
1) Assign the ".flickajax" class to any links where you want to have Ajax.
2) Set the HREF to the desired url of the AJAX call_command.
3) Set the expected JSON dict output and associated messages.
*/

/**************************
SETTINGS
***************************/

// Bootstrap button classes. Not using Bootstrap? Write your own classes. Add here.
var btn_ready_class = 'btn-primary';
var btn_waiting_class = 'btn-warning';
var btn_success_class = 'btn-success';
var btn_error_class = 'btn-danger';


var labijax = {
    'btn-primary': '<i class="fa fa-magnet"></i> Reflick',
    'btn-warning': '<i class="fa fa-spinner fa-spin"></i> Please wait ...',
    'btn-success': '<i class="fa fa-check"></i> Complete',
    'btn-danger': '<i class="fa fa-exclamation"></i> Error',
}

// 'Started process. Please wait. It may timeout... never mind. Refresh later.'
// Click to repeat. Refresh the page to see changes.


/**************************
DO NOT EDIT
***************************/
var btn_classes_all = btn_ready_class + ' ' +
    btn_success_class + ' ' +
    btn_waiting_class + ' ' +
    btn_error_class

$(document).ready(function() {

    // Initialise control buttons
    $('.flickajax').initFlickajax();

    // Add the "click" handler
    $('.flickajax').click(function(e) {
        e.preventDefault();
        $(this).FlickajaxApiCall();
    });

    $('.flickajax').tooltip();

});


jQuery.fn.extend({
    // Make an Ajax call
    FlickajaxApiCall: function(e) {
        var btn = $(this);

        // Check it hasn't already been disabled
        if (!btn.is(':visible')) return;
        if (btn.hasClass('havinojax')) return;

        // Disable it during the AJAX call
        btn.disableFlickajax();
        btn._FlickajaxButtonize(btn_waiting_class, '');

        url = btn.attr('href');

        // Call the API on the URL
        callFlickajax(url, btn);

    },
    // Handles the callback from the Ajax call
    FlickajaxApiCallResp: function(data, msg) {
        var btn = $(this);

        if (!msg) {
            // Reset the button
            btn.enableFlickajax();
            btn._FlickajaxButtonize(btn_success_class, data.report);

        } else {
            // Handle the error
            btn.FlickajaxError(msg || data.report);
        };
    },
    // Ready the button for use
    initFlickajax: function() {
        var btn = $(this);

        if (btn.hasClass('havinojax')) return;

        //btn._FlickajaxButtonize(btn_ready_class, '');

        //Importanty
        // url = btn.attr('href');
        // alert(url)
        // btn.attr('link', url);
        // btn.attr('href', 'javascript:void(0);');

        // For buttons that require us to do the clicking
        // for the user
        btn.enableFlickajax();
    },
    // Set button state after an error
    FlickajaxError: function(err_msg) {
        var btn = $(this);
        btn._FlickajaxButtonize(btn_error_class, err_msg);
        btn.disableFlickajax();
    },
    // Disable the button so it can't be clicked for now
    disableFlickajax: function() {
        var btn = $(this);
        btn.addClass('havinojax');
    },
    // Reenable the button so it can be clicked, etc
    enableFlickajax: function() {
        var btn = $(this);
        btn.removeClass('havinojax');
    },
    _FlickajaxButtonize: function(set_class, bonus_msg) {
        var btn = $(this);
        btn.removeClass(btn_classes_all);
        btn.addClass(set_class);
        btn.html(labijax[set_class]);
        btn.prop('title', bonus_msg);
    },
});

// Utility function to handle the call to Connect API
function callFlickajax(ujax, obj) {

    $.ajax({
            url: ujax,
            type: "GET",
            dataType: "json",
            timeout: 120000,
        })
        .done(function(data, resMsg, jqXHR) {
            //alert(data.Flickajax_state);
            obj.FlickajaxApiCallResp(data, '');
        })
        .fail(function(jqxhr, textstatus, error) {
            obj.FlickajaxApiCallResp('Error', error);
        });
};
