# -*- encoding: utf-8 -*-
"""`FlickrAPIMarshall` simply marshalls common calls through to flickrapi, which
takes care of the rest.

This code is the fruit of 3 years experience using `flickrapi` to build my
pet-project "Hipstamatic Combo Compare" (whose src images are hosted by Flickr).
https://www.in4corners.co.uk

`flickrapi` is so good anyway you hardly need this "rizla" of a class, but I
find it convenient for the following reasons.

    1)  It usefully bookmarks all my regular api calls using a shorthand method
    names.

    2)  Includes the flickrapi calls for "Collections" (see bottom) which are
    not in the official Yahoo documentation.
    https://www.flickr.com/services/api/

    3)  Calls common flickrapi methods without needing all the kwarged
    parameters. The parameter order usually reflects the method name (what comes
    first in the method name, comes first as a parameter).

    4)  Automatically handles login and auth.

    5)  Returns an <object>_id rather than a dictionary object for "create"
    methods, otherwise all methods return flickrapi's response verbatim. I find
    this more useful. If my code just created an object, the ID is the usually
    the only thing I don't know.

Note on the variable names for the three main objects:

When working with an API, I find it helps to have variables names which can be
searched independantly from the API's keyword names. I chose to prefix the names
with an "f" (for Flickr). I find silly names easy to remember.

    - follection (a local instance of a Flickr "Collection") fotoset (a local
    - instance of a Flickr "Album" aka "Photoset" in the api.) foto (a local
    - instance of a Flickr "Photo")

For "photo" and "photoset", using an "f" works great, because, you know, it's
"photo" with a lisp. "follection" is just silly, but a Collection is really just
a kind of folder, + it's consistent and memorable - so there :p

***Usage:***

Add the following to your env vars:

::

    FLICKR_UID = '########@N##6'
    FLICKR_KEY = 'abc123abc123abc123abc133abc123abc'
    FLICKR_SECRET = 'fed098fed098fed'

You need to register your version of this ap with Flickr - who will give you the
information above. These details are your APPs authentication - and the first
time you call the API you will be directed to a webpage so that you can give
your code permission - standard stuff.

If you are calling this code from the command line with no UI, use
`FlickrAPINoAuthAuth`.

Logging in is automatic; once you have set the permission for it.

::

    from flickrapi_toolkit.marshall import FlickrAPIStandardAuth, FlickrAPIMarshall
    futh = FlickrAPIStandardAuth()
    mrs_f = FlickrAPIMarshall(futh)
    Outputs:> Welcome, <your Yahoo user name>. You may proceed.

Execute a call to the Flickr API:

::

    fotoset_id = '1234567890987'
    mrs_f.delete_fotoset(fotoset_id)

If you are only reading data, choose "reading_only=True" when you init the
marshall. This uses a short term cache and will speed things up.

::

    reading_only = True
    mrs_f = FlickrAPIMarshall(futh, reading_only)

If you are intending to create sets and collections, you will need to keep
"reading_only=False". For instance, to create a fotoset and add it to a
follection you need to append its ID to the other fotosets in the same
follection. But if you have just added those other fotosets your cache won't
realise they are there. If not reading_only flickrapi will refresh the data for
each step.

Call a flickrapi method not in the marshall class (most aren't)

::

    mrs_f.flickr_com.<object>.<api_method_name>(*kwargs)

    collection_info = mrs_f.flickr_com.collections.getInfo(
                                    collection_id=follection_id,
                                    )

There are plenty more examples of how to call the flickrapi in
`FlickrAPIToolKit`, and the class below!
"""
import logging

LOG_FILENAME = 'mrs_f.log'
logging.basicConfig(filename=LOG_FILENAME)

import os
import flickrapi
from django.conf import settings


PROTECTED_COLLECTION_IDS = [
                    '58879823-72157666540817130', '58879823-72157650916284099',
                    '58879823-72157647208193996', '58879823-72157644348309268',
                    '58879823-72157634976022140', '58879823-72157644348373337',
                    '58879823-72157644750279422', ]


class ProtectedFollectionException(Exception):

    def __init__(self, fol_id):
        Exception.__init__(self)
        self.fol_id = fol_id

    def __str__(self):
        return repr("""
                    You cannot do this.
                    {} in marshall.PROTECTED_COLLECTION_IDS.
                    """.format(self.fol_id))


def pass_but_log(mess):
    """`FlickrAPIMarshall` is designed to help batch processing, errors are
    largely ignored.
    """
    logging.warning(mess)


class FlickrAPIStandardAuth(object):
    """Authenicate with the Flickr API on a desktop (if browser installed)

    ***Usage:***

    ::

        from flickrapi_toolkit.marshall import FlickrAPIStandardAuth, FlickrAPIMarshall
        futh = FlickrAPIStandardAuth()
        mrs_f = FlickrAPIMarshall(futh)

    """

    def login(self, flicker_com):
        """Called from `FlickrAPIMarshall.reboot()` to authenticate the
        session.`

        ***Usage:***

            1)  `FlickrAPIMarshall` calls `FlickrAPIStandardAuth.login()`
                which will open up your browser.
            2)  Log into your Flickr account (if not already).
            3)  Give the app the permission it requires.

        :param flicker_com: A `FlickrAPIMarshall` instance
        """

        flicker_com.authenticate_via_browser(perms='delete')
        pass


class FlickrAPINoAuthAuth(object):
    """Authenicate with the Flickr API in a remote shell

    ***Usage:***

    ::

        from flickrapi_toolkit.marshall import FlickrAPINoAuthAuth, FlickrAPIMarshall
        futh = FlickrAPINoAuthAuth()
        mrs_f = FlickrAPIMarshall(futh)

    """

    def login(self, flicker_com):
        """Called from `FlickrAPIMarshall.reboot()` to authenticate the
        session.`

        ***Usage:***

            1)  `FlickrAPIMarshall` calls `FlickrAPIStandardAuth.login()`
                and will be prompted for "Verifier code".
            2)  Visit the URL shown in the prompt.
            3)  Log into your Flickr account (if not already).
            4)  Give the app the permission it requires.
            5)  Type the code given into shell prompt.

        :param flicker_com: A `FlickrAPIMarshall` instance
        """

        # Only do this if we don't have a valid token already
        if not flicker_com.token_valid(perms='write'):
            # Get a request token
            flicker_com.get_request_token(oauth_callback='oob')
            # Open a browser at the authentication URL. Do this however
            # you want, as long as the user visits that URL.
            authorize_url = flicker_com.auth_url(perms='write')
            # TODO: decide if you need this still: webbrowser.open_new_tab(authorize_url)
            # Get the verifier code from the user. Do this however you
            # want, as long as the user gives the application the code.
            verifier = str(input('Verifier code {}: '.format(authorize_url)))
            # Trade the request token for an access token
            flicker_com.get_access_token(verifier)
        pass


class FlickrAPIMarshall(object):
    """`FlickrAPIMarshall` is a lightweight wrapper class for the Python
    Flickr API (flickrapi): https://github.com/sybrenstuvel/flickrapi
    """

    def __init__(self, auth_class, reading_only=False):
        """`auth_class` should be an instance of the `FlickrAPIStandardAuth`
        or `FlickrAPINoAuthAuth` class.
        """
        self.auth_class = auth_class
        self.reading_only = reading_only
        self.user = {'user': {'id': '', 'username': {'_content': 'False'}}}
        self.reboot()

    def reboot(self):
        """Called by `FlickrAPIMarshall.__init__()` and a good way to ensure all
        cache is cleared to get fresh data from Flickr API.
        """

        self.flickr_com = flickrapi.FlickrAPI(
            settings.FLICKR_KEY,
            settings.FLICKR_SECRET,
            format='parsed-json',
            cache=False,
        )

        # Allow cache: not advised when writing to the api
        if self.reading_only:
            self.simple_cache = flickrapi.SimpleCache(
                timeout=300,
                max_entries=200
            )
            self.flickr_com.cache = self.simple_cache

        self.auth_class.login(self.flickr_com)

        if not self.user_id:
            self.user = self.get_user()
            if self.user_id:
                print('',
                      '\n',
                      ' ------------| Flickr  Marshall |------------ \n\n',
                      '   Welcome, {}. You are logged in.'.format(
                          self.user_name),
                      '\n',
                      )
                if self.reading_only:
                    print('',
                          '   Reading (cached) mode: You can make changes, but they \n',
                          '   will not be reflected in the local cache.',
                          '\n',
                          )
                print('',
                      '\n',
                      ' -------------------------------------------------------- \n',
                      ' ======================================================== \n',
                      )
            else:
                print('You are not logged in.')
        else:
            print('... cleared cache ...')

        pass

    """INFO"""

    @property
    def user_id(self):
        """Returns the id of the Flickr account holder"""

        return self.user['user']['id']

    @property
    def user_name(self):
        """Returns the name of the Flickr account holder"""

        return self.user['user']['username']['_content']

    def get_user(self):
        """calls `flickr.test.login`"""

        user = self.user

        try:
            user = self.flickr_com.test.login()
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return user

    def follection_info(self, follection_id):
        """FOLLECTION: calls `flickr.collections.getInfo`"""
        follection = {}

        try:
            follection = self.flickr_com.collections.getInfo(
                collection_id=follection_id,
                user_id=settings.FLICKR_UID,
            )
            # write_it(follection, 'follection_info')
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return follection.get('collection', {})

    def follection_tree(self, follection_id):
        """FOLLECTION: calls `flickr.collections.getTree`"""

        follection = {}

        try:
            follection = self.flickr_com.collections.getTree(
                collection_id=follection_id,
                user_id=settings.FLICKR_UID,
            )

            # write_it(follection, 'follection_tree')

        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return follection

    def fotoset_info(self, fotoset_id):
        """FOTOSET: calls `flickr.photosets.getInfo`"""

        fotoset_info = {}
        try:
            fotoset_info = self.flickr_com.photosets.getInfo(
                photoset_id=fotoset_id,
                user_id=settings.FLICKR_UID,
            )
            # write_it(fotoset_info, 'fotoset_info')
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return fotoset_info.get('photoset', {})

    def foto_info(self, foto_id):
        """FOTOSET: calls `flickr.photosets.getInfo`"""

        foto_info = {}
        try:
            foto_info = self.flickr_com.photos.getInfo(
                photo_id=foto_id,
                user_id=settings.FLICKR_UID,
            )
            # write_it(fotoset_info, 'fotoset_info')
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return foto_info.get('photo', {})

    def fotos_of_fotoset(self, fotoset_id):
        """FOTOSET: calls `flicker.photosets.getPhotos`"""

        fotos_list = []
        fotos = {}

        try:
            # noset_fotos = self.flickr_com.photos.getNotInSet(
            #                                     per_page=500,
            #                                     user_id=settings.FLICKR_UID
            #                                     )
            fotos = self.flickr_com.photosets.getPhotos(
                photoset_id=fotoset_id,
                extras='date_taken,tags,original_format',
                user_id=settings.FLICKR_UID,
            )
            # write_it(fotos, 'fotos_of_fotoset')
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        if fotos:
            for foto in fotos['photoset']['photo']:
                fotos_list.append(foto)

        return fotos_list

    def recently_updated_foto(self):
        """FOTO: calls `flicker.photos.recentlyUpdated`"""

        recently_updated = []

        try:
            recently = datetime(now.year, now.month, now.day, now.hour, 0, 0)
            new_fotos = self.flickr_com.photos.recentlyUpdated(
                min_date=recently,
                per_page=10,
                page=1,
            )
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return recently_updated

    """EDIT"""

    def edit_follection(self, follection_id, fitle, frescription=None):
        """FOLLECTION: Undocumented call to `flickr.collections.editMetaOptional`
        """


        follection = {}

        try:
            follection = self.flickr_com.collections.editMeta(
                collection_id=follection_id,
                title=fitle,
                description=frescription,
            )
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return follection

    def iconise_follection(self, foto_ids, follection_id):
        """FOLLECTION: Undocumented call to `flickr.collections.createIconRequired`
        """

        follection = {}

        try:
            follection = self.flickr_com.collections.createIcon(
                photo_ids=foto_ids,
                collection_id=follection_id,
            )
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return follection

    def edit_fotoset(self, fotoset_id, fitle, frescription=None):
        """FOTOSET: calls `flickr.photosets.editMeta`"""

        fotoset = {}

        try:
            fotoset = self.flickr_com.photosets.editMeta(
                photoset_id=fotoset_id,
                title=fitle,
                description=frescription,
            )
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return fotoset

    """CREATE"""

    def make_follection(self, fitle, parent_follection_id=None,
                        frescription=None):
        """FOLLECTION: Undocumented call to `flickr.collections.createRequired`
        TODO: : what is `after_new_coll` optional parameter?
        """

        follection_id = 0
        follection = None

        try:
            follection = self.flickr_com.collections.create(
                title=fitle,
                description=frescription,
                parent_id=parent_follection_id,
                # after_new_coll=after_new_coll,
            )
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        if follection:
            follection = follection.get('collection', {})
            follection_id = follection.get('id', 0)

        return follection_id

    def create_fotoset(self, fitle, frimary_foto_id, frescription=None):
        """FOTOSET: calls `flicker.photosets.create`"""

        fotoset_id = 0

        try:
            fotoset = self.flickr_com.photosets.create(
                title=fitle,
                primary_photo_id=frimary_foto_id,
                description=frescription,
            )
            fotoset_id = fotoset['photoset']['id']
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return fotoset_id

    def upload_foto(self, foto_path, fitle):
        """FOTO: calls `flicker.upload`"""

        foto_id = 0
        foto = None

        try:

            foto = self.flickr_com.upload(
                filename=foto_path,
                title=fitle,
                is_public=0,
                content_type=1,
                format='etree',
            )  # rest etree xmlnode

        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        if foto:
            foto_id = foto.find('photoid').text

        return foto_id

    """DELETION"""

    def delete_follection(self, follection_id, recursive=False):
        """FOLLECTION: Undocumented call to `flickr.collections.deleteRequired`
        """

        if follection_id in PROTECTED_COLLECTION_IDS:
            raise ProtectedFollectionException(follection_id)

        follection = {'stat': 'start'}

        try:
            follection = self.flickr_com.collections.delete(
                collection_id=follection_id,
                recursive=False,
            )

        except flickrapi.FlickrError as ex:
            follection['stat'] = str(ex)
            if 'not found' in follection['stat']:
                follection['stat'] = 'ok'
            else:
                pass_but_log(str(ex))

        return follection.get('stat', 'fail') == 'ok'

    def delete_fotoset(self, fotoset_id):
        """FOTOSET: calls `flickr.photosets.delete`"""

        fotoset = {'stat': 'start'}

        try:
            fotoset = self.flickr_com.photosets.delete(
                photoset_id=fotoset_id,
            )
        except flickrapi.FlickrError as ex:
            fotoset['stat'] = str(ex)
            if 'not found' in fotoset['stat']:
                fotoset['stat'] = 'ok'
            else:
                pass_but_log(str(ex))

        return fotoset.get('stat', 'fail') == 'ok'

    def delete_foto(self, foto_id):
        """FOTO: calls `flickr.photos.delete`"""

        foto = {'stat': 'start'}

        try:
            foto = self.flickr_com.photos.delete(
                photo_id=foto_id,
            )
        except flickrapi.FlickrError as ex:
            foto['stat'] = str(ex)
            if 'not found' in foto['stat']:
                foto['stat'] = 'ok'
            else:
                pass_but_log(str(ex))

        return foto.get('stat', 'fail') == 'ok'

    """MOVE"""

    def move_follection_to_parent_follection(self, follection_id,
                                             parent_follection_id=None):
        """FOLLECTION: Undocumented `flickr.collections.moveCollectionRequired`"""

        follection = {}

        try:
            follection = self.flickr_com.collections.moveCollection(
                collection_id=follection_id,
                parent_collection_id=parent_follection_id,
            )
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return follection

    def remove_fotoset_from_follection(self, fotoset_id, follection_id):
        """FOTOSET: Undocumented `flickr.collections.removeSetRequired`"""

        follection = {}

        try:
            follection = self.flickr_com.collections.removeSet(
                photoset_id=fotoset_id,
                collection_id=follection_id,
            )
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return follection

    def add_foto_to_fotoset(self, foto_id, fotoset_id):
        """FOTO: calls `flicker.photosets.addPhoto`"""

        fotoset = {}

        try:
            fotoset = self.flickr_com.photosets.addPhoto(
                photo_id=foto_id,
                photoset_id=fotoset_id,
            )
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return fotoset

    """SORT"""

    def sort_follections_in_follection(self, follection_ids, follection_id,
                                       no_move=False):
        """FOLLECTION: Undocumented `flickr.collections.sortCollections`"""

        follection = {}

        try:
            follection = self.flickr_com.collections.sortCollections(
                child_collection_ids=follection_ids,
                collection_id=follection_id,
                no_move=no_move,
            )
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return follection

    def sort_fotosets_in_follection(self, fotoset_list, follection_id,
                                    do_remove=False):
        """FOTOSET: Undocumented `flickr.collections.editSetsRequired`"""

        fotosets = ','.join(map(str, fotoset_list))

        follection = {}

        try:
            follection = self.flickr_com.collections.editSets(
                photoset_ids=fotosets,
                collection_id=follection_id,
                do_remove=do_remove,
            )
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return follection

    def sort_fotos_in_fotoset(self, foto_ids, fotoset_id):
        """FOTO: Undocumented `flickr.photosets.reorderPhotosRequired`"""

        fotoset = {}

        try:
            fotoset = self.flickr_com.photosets.reorderPhotos(
                photo_ids=foto_ids,
                photoset_id=fotoset_id,
            )
        except flickrapi.FlickrError as ex:
            pass_but_log(str(ex))

        return fotoset

    """
    ***Notes***

    Undocumented calls used above:

    ::

        flickrapi.collections.create
        flickrapi.collections.sortCollections
        flickrapi.collections.editSets
        flickrapi.collections.createIcon
        flickrapi.collections.removeSet
        flickrapi.collections.delete
        flickrapi.collections.moveCollection
        flickrapi.collections.editMeta
        flickrapi.collections.reorderPhotos

    Details of all documented calls:

    https://www.flickr.com/services/api/
    """
