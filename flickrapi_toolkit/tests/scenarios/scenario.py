# -*- encoding: utf-8 -*-
"""scenario manger"""

import os
from django.conf import settings

from flickrapi_toolkit.models import Foto, Fotoset, Follection
from flickrapi_toolkit.marshall import (
    FlickrAPIStandardAuth,
    PROTECTED_COLLECTION_IDS,
)
from flickrapi_toolkit.toolkit import FlickrAPIToolKit
from flickrapi_toolkit.flickr_sync import (
    FlickrSync,
    SYNC_UPLOADING_IMAGES,
    SYNC_DOWNLOADING_IMAGES,
)


def init_upload_scenario(scenario_name, follection_id=None):
    """Uploads the photo album for this scenario to Flickr.

    :param scenario_name: The name of a folder (containing an image library you
    want to test) inside flickrapi_toolkit.tests.scenarios

    :param follection_id: Looks like '87654321-12345678901234567'. Not required.
    """

    futh = FlickrAPIStandardAuth()
    fync = FlickrSync(
                    futh,
                    follection_id
                    )

    follection_id = fync.sync(SYNC_UPLOADING_IMAGES)

    print('Uploaded {} Scenario @'.format(scenario_name), follection_id)
    return follection_id


def init_rollback_scenario(scenario_name, follection_id=None):
    """Deletes the photos, photosets and collections in this scenario, which
    have previously been uploaded to Flickr.

    :param scenario_name: The name of a folder (containing an image library you
    want to test) inside flickrapi_toolkit.tests.scenarios, e.g. animals26

    :param follection_id: Looks like '87654321-12345678901234567'. Not required.
    """

    futh = FlickrAPIStandardAuth()
    fool = FlickrAPIToolKit(futh)

    for fol in fool.follections_of_follection():
        fol_id = fol['id']
        if not fol_id in PROTECTED_COLLECTION_IDS:
            if fol['title'] == scenario_name:
                fool.fotosets_of_follection(fol_id, True)
                fool.delete_fotos_of_follection(fol_id, True)
                fool.delete_follections_in_follection(fol_id, True)
                fool.mrs_f.delete_follection(fol_id)

    Foto.objects.filter(album=scenario_name).delete()
    Fotoset.objects.filter(album=scenario_name).delete()
    Follection.objects.filter(album=scenario_name).delete()

    print('Rolled back {} Scenario'.format(scenario_name))
