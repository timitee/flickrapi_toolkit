# -*- encoding: utf-8 -*-
import factory


from flickrapi_toolkit.models import *

class FollectionFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Follection

    title = 'FollectionFactory'

    @factory.sequence
    def id(n):
        return '58879823-7215766654081713{}'.format(n)



class FotosetFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Fotoset

    title = 'FotosetFactory'
    id = '72157651235571336'


class FotoFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Foto

    title = 'FotoFactory'
    id = '16788223575'
