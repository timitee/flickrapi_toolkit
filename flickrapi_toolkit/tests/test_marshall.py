# -*- encoding: utf-8 -*-
"""test_marshall

Test the basic functionality of the FlickrApiMarshall.
"""
import pytest
from django.conf import settings

from flickrapi_toolkit.marshall import FlickrAPIMarshall, FlickrAPIStandardAuth


def test_flickr_uid():

    assert settings.FLICKR_UID != 'your flickr uid'
    assert settings.FLICKR_UID


def test_login():

    futh = FlickrAPIStandardAuth()
    mrs_f = FlickrAPIMarshall(futh)

    assert mrs_f.user_id == settings.FLICKR_UID
