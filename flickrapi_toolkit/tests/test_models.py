# -*- encoding: utf-8 -*-
"""test_models

Tests sync scenarios, ensuring the Model data matches the objects they model in
the online Flickr API.
"""
import os
import pytest
from django.conf import settings


from flickrapi_toolkit.tests.scenarios.scenario import init_rollback_scenario
from flickrapi_toolkit.marshall import FlickrAPIStandardAuth
from flickrapi_toolkit.flickr_sync import FlickrSync

from flickrapi_toolkit.models import *
from flickrapi_toolkit.tests.factories import (
    FollectionFactory,
    FotosetFactory,
    FotoFactory,
)

scenario_name = 'animals26'

futh = FlickrAPIStandardAuth()
fync = FlickrSync(
        futh,
        '',
        )


@pytest.mark.django_db
def test_model_root_follection_by_path():

    test_obj = Follection.file_manager.model_by_path(
        root_folder,
        root_folder,
    )

    assert test_obj.title == scenario_name
    assert test_obj.path == ''


@pytest.mark.django_db
def test_model_child_follection_by_path():


    test_obj = Follection.file_manager.model_by_path(
        root_folder,
        os.path.join(root_folder, 'cat'),
    )

    assert test_obj.title == 'cat'
    assert test_obj.path == 'cat'
    assert test_obj.album == scenario_name


@pytest.mark.django_db
def test_model_fotoset_by_path():

    test_obj = Fotoset.file_manager.model_by_path(
        root_folder,
        os.path.join(root_folder, 'cat', 'cat'),
    )

    assert test_obj.title == 'cat'
    assert test_obj.path == 'cat/cat'
    assert test_obj.album == scenario_name


@pytest.mark.django_db
def test_model_fotoset_by_path():

    test_obj = Fotoset.file_manager.model_by_path(
        root_folder,
        os.path.join(root_folder, 'cat', 'cat', 'cat.jpg'),
    )

    assert test_obj.title == 'cat.jpg'
    assert test_obj.path == 'cat/cat/cat.jpg'
    assert test_obj.album == scenario_name



@pytest.mark.django_db
def test_sync_root_follection():

    test_obj = Follection.file_manager.sync(
                                        fync,
                                        root_folder,
                                        ''
                                        )

    assert test_obj.id
    assert len(test_obj.id) == 26
    assert test_obj.title == scenario_name
    assert test_obj.path == ''
    assert test_obj.album == scenario_name

    fol_id = test_obj.id

    test_obj = Follection.file_manager.reset(
                                        fync,
                                        root_folder,
                                        ''
                                        )

    obj_found = fync.mrs_f.follection_info(fol_id)
    assert not obj_found


@pytest.mark.django_db
def test_sync_any_follection():

    obj_folder = 'bird/singing'
    deeper_than_root = os.path.join(root_folder, obj_folder)

    test_obj = Follection.file_manager.sync(
                                        fync,
                                        deeper_than_root,
                                        ''
                                        )

    assert len(test_obj.id) == Follection._meta.get_field('id').max_length
    assert test_obj.title == os.path.split(obj_folder)[1]
    assert test_obj.path == obj_folder
    assert test_obj.follection.title == obj_folder.split(os.sep)[-2]
    assert test_obj.follection.follection.title == scenario_name
    assert test_obj.album == scenario_name

    fol_id = test_obj.id
    p_fol_id = test_obj.follection.id
    gp_fol_id = test_obj.follection.follection.id

    Follection.file_manager.reset(
                                        fync,
                                        deeper_than_root,
                                        ''
                                        )

    obj_found = fync.mrs_f.follection_info(fol_id)
    assert not obj_found

    obj_found = fync.mrs_f.follection_info(p_fol_id)
    assert obj_found

    obj_found = fync.mrs_f.follection_info(gp_fol_id)
    assert obj_found

    init_rollback_scenario(scenario_name)


@pytest.mark.django_db
def test_sync_any_fotoset():

    obj_folder = 'bird/singing/singing'
    deeper_than_root = os.path.join(root_folder, obj_folder)

    primary_image = FotoFactory()

    test_obj = Fotoset.file_manager.sync(
                                        fync,
                                        deeper_than_root,
                                        primary_image,
                                        ''
                                        )

    assert len(test_obj.id) == Fotoset._meta.get_field('id').max_length
    assert test_obj.title == os.path.split(obj_folder)[1]
    assert test_obj.path == obj_folder

    assert test_obj.follection.title == obj_folder.split(os.sep)[-2]
    assert test_obj.follection.follection.title == obj_folder.split(os.sep)[-3]
    assert test_obj.follection.follection.follection.title == test_obj.album
    assert test_obj.album == scenario_name

    fs_id = test_obj.id
    p_fol_id = test_obj.follection.id
    gp_fol_id = test_obj.follection.follection.id
    ggp_fol_id = test_obj.follection.follection.follection.id

    Fotoset.file_manager.reset(
                                        fync,
                                        deeper_than_root,
                                        ''
                                        )

    obj_found = fync.mrs_f.fotoset_info(fs_id)
    assert not obj_found

    obj_found = fync.mrs_f.follection_info(p_fol_id)
    assert obj_found

    obj_found = fync.mrs_f.follection_info(gp_fol_id)
    assert obj_found

    obj_found = fync.mrs_f.follection_info(ggp_fol_id)
    assert obj_found

    init_rollback_scenario(scenario_name)


@pytest.mark.django_db
def test_sync_any_foto():

    obj_folder = 'bird/singing/singing/canary.gif'
    deeper_than_root = os.path.join(root_folder, obj_folder)

    test_obj = Foto.file_manager.sync(
                                        fync,
                                        deeper_than_root,
                                        ''
                                        )

    assert len(test_obj.id) == Foto._meta.get_field('id').max_length
    assert test_obj.title == 'canary'
    assert test_obj.file_type == '.gif'
    assert test_obj.path == obj_folder

    assert len(test_obj.fotoset.id) == Fotoset._meta.get_field('id').max_length
    assert test_obj.fotoset.title == 'singing'

    assert test_obj.follection.title == obj_folder.split(os.sep)[-3]
    assert test_obj.follection.follection.title == obj_folder.split(os.sep)[-4]
    assert test_obj.follection.follection.follection.title == test_obj.album
    assert test_obj.album == scenario_name

    foto_id = test_obj.id
    fs_id = test_obj.fotoset.id
    p_fol_id = test_obj.follection.id
    gp_fol_id = test_obj.follection.follection.id
    ggp_fol_id = test_obj.follection.follection.follection.id

    Foto.file_manager.reset(
                                        fync,
                                        obj_folder,
                                        ''
                                        )

    obj_found = fync.mrs_f.fotoset_info(fs_id)
    assert obj_found

    obj_found = fync.mrs_f.follection_info(p_fol_id)
    assert obj_found

    obj_found = fync.mrs_f.follection_info(gp_fol_id)
    assert obj_found

    obj_found = fync.mrs_f.follection_info(ggp_fol_id)
    assert obj_found

    obj_found = fync.mrs_f.foto_info(foto_id)
    assert not obj_found

    init_rollback_scenario(scenario_name)
