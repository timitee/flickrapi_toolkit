# -*- encoding: utf-8 -*-
"""test_flickr_sync

Test the integrity of flickr_sync
"""

import os
import pytest
from django.conf import settings

from flickrapi_toolkit.tests.scenarios.scenario import init_upload_scenario
from flickrapi_toolkit.tests.scenarios.scenario import init_rollback_scenario

from flickrapi_toolkit.marshall import FlickrAPIStandardAuth
from flickrapi_toolkit.flickr_sync import FlickrSync

# scenario_name = 'animals26'

# futh = FlickrAPIStandardAuth()
# fync = FlickrSync(
#         futh,
#         '',
#         )
#
# @pytest.mark.django_db
# def test_parent_folder_name():
#
#     root = os.path.join(root_folder, 'fish/flat')
#     assert fync.parent_folder_name(root, 'plaice.gif') == 'flat'
