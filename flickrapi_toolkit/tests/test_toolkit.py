# -*- encoding: utf-8 -*-
"""test_toolkit

Test the rebustness of the FlickrAPIToolKit's parsing methods - which flattens
out the Collection tree. Also test multi step methods.
"""
import os
import pytest
from django.conf import settings

from flickrapi_toolkit.marshall import FlickrAPIStandardAuth
from flickrapi_toolkit.toolkit import FlickrAPIToolKit
