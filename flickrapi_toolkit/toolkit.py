# -*- encoding: utf-8 -*-
"""The main purpose of `FlickrAPIToolKit` is to simplify handling Flickr API's
(https://www.flickr.com/services/api/) returned data. You can use this class to
more easily traverse the tree structure of your Flickr account.

FlickrAPIToolKit also contains methods to handle common multistep operations,
like create a Photoset and put it into Collection; or move a tree of Collections
into a new parent Collection.

The Toolkit has it's own internal `FlickrAPIMarshall` to marshall flickrapi's
calls.

***Usage:***

::

    from flickrapi_toolkit.marshall import FlickrAPIMarshall, FlickrAPIStandardAuth
    from flickrapi_toolkit.toolkit import FlickrAPIToolKit
    futh = FlickrAPIStandardAuth()
    fool = FlickrAPIToolKit(futh)

"""


import os
import random
from flickrapi_toolkit.marshall import FlickrAPIMarshall
from flickrapi_toolkit.service import (
    FlickrAPIToolKitError,
    FlickrAPISorter,
    truncate,
)


class FlickrAPIToolKit(object):
    """Toolkit for parsing Flickr API data and performing common tasks"""


    def __init__(self, futh, reading_only=False):
        """
        :param futh: An instance of the `FlickrAPIStandardAuth` or
        `FlickrAPINoAuthAuth` class.

        :param reading_only: False otherwise flickrapi will cache reads from
        Flickr account, and any changes the Toolkit makes won't be reflected.
        """

        # Authentication happens when you instantiate the FlickrAPIMarshall
        self.mrs_f = FlickrAPIMarshall(futh, reading_only)
        """`FlickrAPIToolKit.mrs_f` is an instance of `FlickrAPIMarshall`"""


    """INFO UTILS"""

    def _follection_root(self, follection_id=None):
        """`FlickrAPIToolKit._follection_root()` scraps a Flickr's tree of
        follections and returns children or descendants as a flat list, where
        each item holds its children or descendants, even when all descendants
        are also in the root list.
        """

        follection_tree = self.mrs_f.follection_tree(follection_id)

        follection_root = follection_tree.get('collections', [])

        if follection_id:
            """Important:

            getTree['collections']['collection'] return something like this:

            ::

                root['collections']['collection'] =
                    {'id': 'Root',
                      'collection': [
                        >># {'id': 'Root',
                             'collection': [
                                {'id': 'x', ...},
                                {'id': 'y', ...},
                                {'id': 'z', ...},
                                ]}  #<<<--- this is a nested copy of its parent
                            {'id': 'x', ...},
                            {'id': 'y', ...},
                            {'id': 'z', ...},
                        ]
                    }

            Root's first child collection is "Root". Excluding itself,
            the first child's collections are the same as Roots.

            It's why we only ever use the first "collection" of getTree's data.
            """
            follection_root = follection_root['collection'][0] if follection_root else []

        return follection_root


    def _descendants_of_follections(self,
            follection_id=None, descendants_type='collection', go_deep=False):
        """FOLLECTION: Get a follection's child or descendants; `go_deep` for a
        flat list of all descendants. This is the heart of `FlickrAPIToolKit`.

        :param descendants_type: (Optional) 'collection/set'.
        :param go_deep: (Optional) True to return all descendants.
        """

        follection_root = self._follection_root(follection_id)

        if not follection_root:
            return follection_root

        if go_deep:
            return follection_descendants(
                                            follection_root,
                                            descendants_type
                                            )
        else:
            return follection_children(
                                            follection_root,
                                            descendants_type
                                            )


    """INFO"""
    def has_follection(self, follection_id=None, go_deep=False):

        return bool(self.mrs_f.follection_info(follection_id))


    def follections_of_follection(self, follection_id=None, go_deep=False):
        """FOLLECTION: Returns a flat list of all child or descendant
        follections of a follection.

        :param go_deep: (Optional) True to return all descendants.
        """

        return self._descendants_of_follections(
                                            follection_id,
                                            'collection',
                                            go_deep,
                                            )


    def fotosets_of_follection(self, follection_id, go_deep=False):
        """FOTOSET: Returns a flat list of all children or descendant fotosets
        of a follection.


        :param go_deep: (Optional) True to return all descendants.
        """
        return self._descendants_of_follections(
                                            follection_id,
                                            'set',
                                            go_deep,
                                            )

    def has_follection(self, follection_id):

        return bool(self.mrs_f.follection_info(follection_id))

    def has_fotoset(self, fotoset_id):

        return bool(self.mrs_f.fotoset_info(fotoset_id))

    def has_foto(self, foto_id):

        return bool(self.mrs_f.foto_info(foto_id))


    def confirm_fotoset_in_follection(self, fotoset_id, follection_id,
            go_deep=False):
        """FOTOSET: Returns true or False depending on whether the fotoset is
        is a child or descendant of a follection.

        :param go_deep: (Optional) True to return all descendants.
        """
        fotosets = self.fotosets_of_follection(
                                            follection_id,
                                            go_deep
                                            )
        for fotoset in fotosets:
            if fotoset['id'] == str(fotoset_id):
                return True

        return False


    def foto_id_recently_uploaded(self, fitle):
        """FOTO: Returns the foto_id of a recent image you uploaded when you
        know the `fitle`.
        """

        foto_id = None

        recently_updated = self.mrs_f.recently_updated_foto()

        if recently_updated:
            for ru in recently_updated['photos']['photo']:
                if ru['title'] == fitle:
                    return ru['id']

        return foto_id


    """DELETING"""


    def delete_follections_in_follection(self, follection_id, go_deep=False):
        """FOLLECTION: Deletes all the child or descendant follections in a
        follection.

        :param go_deep: (Optional) True to return all descendants.
        """

        if not follection_id:
            raise FlickrAPIToolKitError(
                            'No collection_id provided',
                            'Not allowing you to delete all your collections!'
                            )

        follections = self.follections_of_follection(
                                            follection_id,
                                            go_deep
                                            )

        for follection in follections:

            follection_id = follection['id']
            self.mrs_f.delete_follection(
                                            follection_id,
                                            False
                                            )

        pass


    def delete_fotosets_in_follection(self, follection_id, go_deep=False):
        """FOTOSET: Deletes all the child or descendant fotosets in a
        follection.

        :param go_deep: (Optional) True to return all descendants.
        """

        if not follection_id:
            raise FlickrAPIToolKitError(
                            'No collection_id provided',
                            'Not allowing you to delete all your photosets!'
                            )

        fotosets = self.fotosets_of_follection(
                                            follection_id,
                                            go_deep
                                            )
        for fotoset in fotosets:
            fotoset_id = fotoset['id']
            self.mrs_f.delete_fotoset(fotoset_id)

        pass


    def delete_fotos_of_follection(self, follection_id, go_deep=False):
        """FOTO: Deletes all the child or descendant fotos in a
        follection.

        :param go_deep: (Optional) True to return all descendants.
        """

        if not follection_id:
            raise FlickrAPIToolKitError(
                            'No collection_id provided',
                            'Not allowing you to delete all your photos!'
                            )

        fotosets = self.fotosets_of_follection(
                                            follection_id,
                                            go_deep
                                            )

        for fotoset in fotosets:
            fotoset_id = fotoset['id']
            self.delete_fotos_of_fotosets(fotoset_id)

        pass


    def delete_fotos_of_fotosets(self, fotoset_id):
        """FOTO: Deletes all the child fotos in a fotoset."""

        fotos = self.mrs_f.fotos_of_fotoset(fotoset_id)
        for foto in fotos:
            foto_id = foto['id']
            self.mrs_f.delete_foto(foto_id)

        pass


    """SORTING"""


    def sort_follection(self, follection_id, sort_reversed=False):
        """FOLLECTION: Sorts a follection's  child follections into alphabetical
        order, then calls the Flick API to sort them.
        """
        s = FlickrSorter()

        follections = self._descendant_collections_of_collection(follection_id)

        for follection in follections:
            fitle = follection['title']
            id = follection['id']
            s.add(fitle, id)

        sorted_ids = s.sorted_ids(sort_reversed)

        self.mrs_f.sort_follections_in_follection(
                                            sorted_ids,
                                            follection_id
                                            )


    def sort_fotosets(self, follection_id, sort_reversed=False):
        """FOLLECTION: Sorts a follection's child fotosets into alphabetical
        order, then calls the Flick API to sort them.
        """

        s = FlickrSorter()

        fotosets = self.fotosets_of_follection(follection_id)

        for fotoset in fotosets:
            id = fotoset['id']
            fitle = fotoset['title']
            s.add(fitle, id)

        fotoset_list = s.sorted_ids(sort_reversed)

        self.mrs_f.sort_fotosets_in_follection(
                                            fotoset_list,
                                            follection_id
                                            )

        pass


    def sort_fotos(self, fotoset_id, sort_reversed=False):
        """FOLLECTION: Sorts a fotoset's child fotos into alphabetical
        order, then calls the Flick API to sort them.
        """

        s = FlickrSorter()

        fotoset = self.mrs_f.fotoset_info(fotoset_id)
        frimary_id = fotoset['photoset']['primary']
        fotos = self.mrs_f.fotos_of_fotoset(fotoset_id)

        for foto in fotos:
            id = foto['id']
            fitle = foto['title']
            is_frimary = foto['isprimary']
            date_taken = foto['datetaken']

            # Fix it so the primary photo is sorted first.
            if frimary_id == id:
                fitle = '####-{}'.format(fitle)

            # Fix it so that my special files are sorted second
            if 'in4-' in fitle:
                fitle = '££££-{}'.format(fitle)

            s.add(fitle, id)

        sorted_ids = s.sorted_ids(sort_reversed)

        self.mrs_f.sort_fotos_in_fotoset(
                                            sorted_ids,
                                            fotoset_id
                                            )

        pass


    """MOVING and COPYING"""


    def copy_fotosets_to_follection(self, from_follection_id, to_follection_id,
            go_deep=False):
        """FOTOSET: Copies all child or descendent fotosets from one follection
        into a another.

        :param go_deep: (Optional) True to return all descendants.
        """
        fotosets = self.fotosets_of_follection(
                                            from_follection_id,
                                            go_deep
                                            )
        fotoset_list = []

        for fotoset in fotosets:
            fotoset_id = fotoset['id']
            fotoset_list.append(fotoset_id)

        self.mrs_f.sort_fotosets_in_follection(
                                            fotoset_list,
                                            follection_id
                                            )

        pass


    def copy_homeless_fotosets_to_follection(self, follection_id):
        """FOTOSET: Copies all homeless fotosets into a follection."""

        fotosets = self.fotosets_of_follection(
                                            from_follection_id,
                                            go_deep
                                            )

        fotoset_list = []
        for fotoset in fotosets:
            fotoset_id = fotoset['id']
            fotoset_list.append(fotoset_id)

        self.mrs_f.sort_fotosets_in_follection(
                                            fotoset_list,
                                            follection_id
                                            )

        pass


    def add_fotoset_to_follection(self, fotoset_id, follection_id):
        """FOTOSET: Puts a fotoset into a follection, which requires first
        getting a list of its current children and adding this one to the list.
        """

        fotosets = self.fotosets_of_follection(follection_id)

        fotoset_list = [f['id'] for f in fotosets] + [fotoset_id]

        if follection_id and fotoset_id:

            self.mrs_f.sort_fotosets_in_follection(
                                            fotoset_list,
                                            follection_id
                                            )

        return fotoset_id


    def create_fotoset_in_follection(self, fitle, frimary_foto_id,
            follection_id=None):
        """FOTOSET: Creates a fotoset and then calls
        `FlickrAPIToolKit.add_fotoset_to_follection()`.
        """
        fotoset_id = self.mrs_f.create_fotoset(fitle, frimary_foto_id)

        if follection_id and fotoset_id:
            self.add_fotoset_to_follection(
                                            fotoset_id,
                                            follection_id
                                            )

        return fotoset_id


    """MOSAICS"""


    def mosaic_follections_of_follection(self, follection_id, go_deep=False):
        """FOLLECTION: This automatically sets the mosaics of all the child or
        descendent follections of a follection.

        :param go_deep: (Optional) True to return all descendants.
        """

        follections = self.follections_of_follection(
                                            follection_id,
                                            go_deep
                                            )

        for follection in follections:
            id = follection['id']
            mosaic = self.mosaic_follection(id)

        return mosaic



    def mosaic_follection(self, follection_id):
        """FOLLECTION: This automatically sets the mosaic of a follection to the
        first fotos it finds."""

        s = FlickrSorter()

        frimary_id = 0
        fotosets = self.fotosets_of_follection(
                                            follection_id,
                                            True
                                            )

        for fotoset in fotosets:

            fotoset_id = fotoset['id']
            frimary_id = fotoset['photoset']['primary']

            fotos = self.mrs_f.fotos_of_fotoset(fotoset_id)
            fotoset_info = self.mrs_f.fotoset_info(fotoset_id)


            for foto in fotos:

                fitle = foto['title']

                if not 'in4-' in fitle:

                    id = foto['id']
                    date_taken = foto['datetaken']
                    if frimary_id == id: fitle = '#'
                    s.add(fitle, id)
                    if s.count() == 12:
                        sorted_ids = s.sorted_ids()
                        rtn = self.iconise_follection(
                                            follection_id,
                                            sorted_ids
                                            )
                        return rtn.get('stat', False)

        while s.count() < 12:
            s.add('£', frimary_id)

        sorted_ids = s.sorted_ids()
        rtn = self.mosaic_follection(
                                            follection_id,
                                            sorted_ids
                                            )

        return rtn.get('stat', False)



def follection_finder(follection_id, follections_of_follection):
    for foll in follections_of_follection:
        if foll['id'] == follection_id:
            return foll
    return None

def fotoset_finder(fotoset_id, fotosets_of_follection):
    for fs in fotosets_of_follection:
        if fs['id'] == fotoset_id:
            return fs
    return None

def follection_children(parent_follection, descendants_type='collection'):
    """Returns the child fotosets or follections of a Flickr Collection.

    :type parent_follection: JSON
    :param parent_follection: JSON from `FlickrAPIMarshall.follection_tree()`
    :param descendants_type: 'collection/set'
    """

    children = []

    # get the required type of descendants
    descendants_list = parent_follection.get(descendants_type, [])

    if type(descendants_list) == list and len(descendants_list) > 0:

        parent_title = parent_follection.get('title', '')
        parent_path = parent_follection.get('path', '')
        parent_breadcrumbs = parent_follection.get('breadcrumbs', [])

        for descendant in descendants_list:
            descendant['parent'] = parent_follection
            descendant['breadcrumbs'] = parent_breadcrumbs.copy()
            descendant['breadcrumbs'].append(parent_follection)
            descendant['path'] = os.path.join(parent_path, descendant['title'])
            children.append(descendant)

    return children


def follection_descendants(parent_follection, descendants_type='collection'):
    """Returns a flat list of all fotosets or follections which are
    ancestors of a Flickr Collection. Even when the descendant_type is a 'set',
    the function will always recusively follow all Sub Collections looking
    for every set it can find.

    :type parent_follection: JSON
    :param parent_follection: JSON from `FlickrAPIMarshall.follection_tree()`
    :param descendants_type: 'collection/set'
    """

    # get the immediate children
    descendants = follection_children(
                                                parent_follection,
                                                descendants_type
                                                )

    parent_title = parent_follection.get('title', '')
    parent_path = parent_follection.get('path', '')
    parent_breadcrumbs = parent_follection.get('breadcrumbs', [])


    # now recursively follow the collections
    follections_list = parent_follection.get('collection', [])
    if type(follections_list) == list and len(follections_list) > 0:
        for follection in follections_list:
            follection['parent'] = parent_follection
            follection['breadcrumbs'] = parent_breadcrumbs.copy()
            follection['breadcrumbs'].append(parent_follection)
            follection['path'] = os.path.join(parent_path, follection['title'])
            descendants += follection_descendants(
                                                follection,
                                                descendants_type
                                                )

    return descendants
