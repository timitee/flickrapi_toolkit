# -*- encoding: utf-8 -*-
"""Models Flickr Collections, Photosets, and Photos - used by Flickr Sync
to keep a file system object and your Flickr Account synced.
"""

import os
import datetime
from django.db import models
from django_extensions.db.fields import AutoSlugField
from flickrapi_toolkit.marshall import FlickrAPIStandardAuth, FlickrAPIMarshall
from flickrapi_toolkit.service import foto_str_to_date, truncate


class SyncObjectManager(models.Manager):
    """For accessing models from the file system."""


    def model_by_path(self, root_path, obj_path):
        """Returns a model instance matching the `obj_path`. If a saved object
        matching this path is not found, an unsaved model instance with this
        path is returned which you can update and save.


        :param root_path:   The root path to all your photos. This is not
                            stored, which means you can move your album to
                            a new location and all the relative paths will
                            still work.

        :param obj_path:    The full path to the directory or image. The
                            path is a primary key.

                            `SyncObjectManager.model_by_path()` will remove
                            the `root_path` and save the relative path.

        """

        obj_path = obj_path.replace(root_path, '')

        if obj_path == os.sep:
            obj_path = ''

        if len(obj_path) > 1 and obj_path[0] == os.sep:
            obj_path = obj_path[1:]

        try:
            obj = self.model.objects.get(path=obj_path)
        except self.model.DoesNotExist:
            obj = self.model(path=obj_path)

        # Pull out the album and title
        obj.album = os.path.basename(root_path)
        obj.title = os.path.basename(obj_path) if obj_path else obj.album

        return obj


    def find_or_model(self, root_path, obj_path, id=None):
        """Returns a model instance matching the path or ID of this object.
        If a matching object is not found, an unsaved model instance is
        returned which can be used when you are syncing, uploading, or
        downloading from your Flickr account.


        :param root_path:   The root path of your album. This is not stored,
                            which means you can move your album to a new
                            location.

        :param obj_path:    The path to this directory or file.

                            `SyncObjectManager.model_by_path()` will remove
                            the `root_path` and save the relative path.

        :param id:          The Flickr ID of a photo, photoset or collection.

                            Uploading (Optional). SyncObjectManager should update
                            the ID when the object has been successfully uploaded.

                            Downloading (Required). SyncObjectManager should use
                            the ID to download the object to the given path.

        """

        # Find a database entry to match
        if id:
            # see if known by id
            obj = self.model.objects.model_by_id(id)

        else:



            # see if known by path
            obj = self.model.file_manager.model_by_path(
                                            root_path,
                                            obj_path,
                                            )
        return obj


    def flickr_attribute(self, fson, attribute):
        api_name = self.model().api_name
        rtn = ''
        try:
            rtn = fson[attribute]
        except:
            pass
        if not rtn:
            try:
                rtn = fson[api_name][attribute]
            except:
                pass
        if not rtn or isinstance(rtn, dict):
            try:
                rtn = fson[attribute]['_content']
            except:
                pass
        if not rtn or isinstance(rtn, dict):
            try:
                rtn = fson[api_name][attribute]['_content']
            except:
                pass
        return rtn


    def get_obj_path(self, parent, title):
        # The root Collection has no path. The path is
        # relative to a root_foll, and the root Collection
        # is the same as the path to the root_foll
        if parent:
            parent_path = parent.path
            return os.path.join(parent_path, title)
        return ''


    def capture_json(self, fson, follection=None):
        """Captures Flick API's JSON response then saves and returns a
        `Fotoset` or `Follection` model instance.

        :param fson: The JSON object of a collection returned by the Flick API.
        :param follection: (Optional) A known parent collection for this object.
        """

        id = self.flickr_attribute(fson, 'id')
        obj = self.model.objects.model_by_id(id)

        obj_title = self.flickr_attribute(fson, 'title')
        if not obj.title:
            obj.title = obj_title

        obj.description = self.flickr_attribute(fson, 'description')

        obj.follection = follection

        if not obj.path:
            obj.path = self.get_obj_path(follection, obj.title)

        return obj


class FlickrObjectManager(models.Manager):
    """For accessing models through DB references and keys."""

    def model_by_id(self, id):
        """Returns the model instance for an object matching this ID.

        :param id: The Flickr ID of a photo, photoset or collection.
        """

        try:
            obj = self.model.objects.get(id=id)
        except self.model.DoesNotExist:
            obj = self.model(id=id)

        return obj


class FlickrObject(models.Model):
    """Standard fields shared by all Fotos, Fotosets, and Follections."""

    title = models.CharField(max_length=255)
    description = models.TextField()
    slug = models.SlugField(max_length=255)
    path =  models.CharField(max_length=255, blank=True, null=True,)
    follection = models.ForeignKey(
        'Follection',
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    album = models.CharField(max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True

    @property
    def api_name(self):
        return self._meta.verbose_name.lower()

    def slug_unique(self):
        return '{} {}'.format(self.verbose_name, self.title)


class SyncFollectionManager(SyncObjectManager):
    """Models syncing file system directories as Collections to Flickr."""


    def sync(self, toolkit, obj_path, id=None):
        """Syncs this directory path to your Flickr Account as a Collection
        and returns the model instance.

        :param toolkit: Execute Flickr API calls against this.

        :param obj_path: This object's path which is a unique key to the
         object coming from the file system.

        :param id: The Flickr ID of a photo, photoset or collection.

            -   Uploading (Optional): SyncObjectManager will
                update the ID when the object has been
                successfully uploaded.

            -   Downloading (required). SyncObjectManager will
                use the ID to download the object to the given
                path.
        """

        # Find or model by id or path
        obj = self.find_or_model(toolkit.root_foll_dir, obj_path, id)

        # Find the parent for this object

        if not toolkit.root_foll_dir == obj_path and obj_path:
            follection_path = os.path.split(obj_path)[0]
            obj.follection = Follection.file_manager.sync(
                                            toolkit,
                                            follection_path
                                            )

        # If only modelled ...
        if not obj.id:
            # ... upload to flickr; assign the id
            obj.id = toolkit.mrs_f.make_follection(
                                            obj.title,
                                            obj.follection_id
                                            )
        # Save and return
        try:
            if obj.id:
                obj.save()
        except:
            toolkit.mrs_f.delete_follection(obj.id)
        # and return
        return obj


    def reset(self, toolkit, obj_path, id=None, recursive=False):
        """An Follection instance of this deleted Flickr object.

        :param toolkit: As above for `SyncFollectionManager.sync()`.
        :param obj_path: As above for `SyncFollectionManager.sync()`.
        :param id: As above for `SyncFollectionManager.sync()`.
        """

        obj = self.find_or_model(toolkit.root_foll_dir, obj_path, id)

        if obj.id:
            if toolkit.mrs_f.delete_follection(obj.id, recursive):
                obj.delete()

        return obj


    def capture_json(self, fson, follection=None):
        """Captures Flick API's JSON response then saves and returns a
        `Follection` model instance.

        :param fson: The JSON object of a collection returned by the Flick API.
        :param follection: (Optional) A known parent collection for this object.
        """
        obj = super().capture_json(fson, follection)
        obj.save()
        return obj


class FollectionManager(FlickrObjectManager):
    """Standard objects manager for `Follection`."""

    def root_follection(self):
        root = self.model.objects.filter(follection_id=None)
        if root:
            return root[0]
        return None


class Follection(FlickrObject):
    """Represents a synced system directory synced to Flickr as a Collection """

    id = models.CharField(primary_key=True, max_length=26, editable=True)

    objects = FollectionManager()
    file_manager = SyncFollectionManager()

    class Meta:
        verbose_name = 'Collection'
        verbose_name_plural = 'Collections'


class SyncFotosetManager(SyncObjectManager):
    """Models syncing file system directories as Photosets to Flickr."""


    def sync(self, toolkit, obj_path, primary_foto, id=None):
        """Syncs this directory path to your Flickr Account as a Photoset
        and returns the model instance.

        :param toolkit: As above for `SyncFollectionManager.sync()`.
        :param obj_path: As above for `SyncFollectionManager.sync()`
        :param primary_foto: The photoset's `primary_foto`.
        :param taken: The primary image's `taken` date.
        :param id: As above for `SyncFollectionManager.sync()`
        """

        # Find or model by id or path
        obj = self.find_or_model(toolkit.root_foll_dir, obj_path, id)

        obj.primary = primary_foto

        if not obj.primary.id:
            raise Exception("Fotoset requires a primary image.")

        # Create the photoset if no ID and there is an image for it.
        if not obj.id:
            # ... upload to flickr; assign the id
            obj.id = toolkit.mrs_f.create_fotoset(
                                            obj.title,
                                            obj.primary.id
                                            )
            print(obj.id)

        # Find the parent for this object
        follection_path = os.path.split(obj_path)[0]
        if not follection_path == obj_path:

            obj.follection = Follection.file_manager.sync(
                                            toolkit,
                                            follection_path
                                            )

        # Move into a follection
        if obj.id and obj.follection.id:
            toolkit.add_fotoset_to_follection(
                                            obj.id,
                                            obj.follection.id
                                            )

        # Date Updated
        obj.date_update = datetime.datetime.now().date()

        # Save and return
        try:
            if obj.id:
                obj.save()
        except:
            toolkit.mrs_f.delete_fotoset(obj.id)

        # And return
        return obj


    def reset(self, toolkit, obj_path, id=None):
        """An Fotoset instance of this deleted Flickr object.

        :param toolkit: As above for `SyncFollectionManager.sync()`.
        :param obj_path: As above for `SyncFollectionManager.sync()`.
        :param id: As above for `SyncFollectionManager.sync()`.
        """

        obj = self.find_or_model(toolkit.root_foll_dir, obj_path, id)

        if obj.id:
            if toolkit.mrs_f.delete_fotoset(obj.id):
                obj.delete()

        return obj


    def capture_json(self, fson, follection=None):
        """Captures Flick API's JSON response then saves and returns a
        `Fotoset` model instance.

        :param fson: The JSON object of a photoset returned by the Flick API.
        :param follection: (Optional) A known parent collection for this object.
        """
        obj = super().capture_json(fson, follection)

        # Primary photo
        primary_foto_id = self.flickr_attribute(fson, 'primary')
        primary = Foto.objects.model_by_id(primary_foto_id)
        primary.save()
        obj.primary = primary

        obj.save()
        return obj


class FotosetManager(FlickrObjectManager):
    """Standard objects manager for `Fotoset`."""
    pass


class Fotoset(FlickrObject):
    """Represents a synced system directory synced to Flickr as a Fotoset """

    id = models.CharField(primary_key=True, max_length=17, editable=True)
    primary = models.ForeignKey(
        'Foto',
        related_name="photosets",
        related_query_name="photoset",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    created = models.DateField(auto_now=False, auto_now_add=True)
    updated = models.DateField(auto_now=True, auto_now_add=False)

    objects = FotosetManager()
    file_manager = SyncFotosetManager()

    class Meta:
        verbose_name = 'Photoset'
        verbose_name_plural = 'Photosets'


class SyncFotoManager(SyncObjectManager):
    """Models syncing file system directories as Photo to Flickr."""

    def sync(self, toolkit, obj_path, id=None):
        """Syncs this directory path to your Flickr Account as a Photo
        and returns the model instance.

        :param toolkit: As above for `SyncFollectionManager.sync()`.
        :param obj_path: As above for `SyncFollectionManager.sync()`
        :param taken: The image's `taken` date.
        :param id: (Optional) As above for `SyncFollectionManager.sync()`
        """

        # If the file is a link/shortcut
        # link_path = None
        # if os.path.islink(obj_path):
        #
        #     # Sync the original file
        #     link_path = obj_path
        #     obj_path = os.readlink(obj_path)

        # find or model by id or path
        obj = self.find_or_model(toolkit.root_foll_dir, obj_path, id)



        if not obj.id:
            # Not uploaded yet

            file_path = os.path.join(toolkit.root_foll_dir, obj.path)
            # upload to flickr
            obj.id = toolkit.mrs_f.upload_foto(
                                            file_path,
                                            obj.title
                                            )

        # Remove the file extension
        obj.file_type = os.path.splitext(obj.title)[1]
        obj.title = os.path.splitext(obj.title)[0]

        # TODO: File rename - let's not do this. What say?
        # if obj.id:
        #     if not obj.id in obj.path:
        #         obj.path = obj.file_path_with_id


        # obj.taken = taken

        # save and pop into fotoset and return
        try:
            if obj.id:
                obj.save()
                obj.sync_to_fotoset(toolkit)
        except:
            toolkit.mrs_f.delete_foto(obj.id)



        # and return
        return obj




    def reset(self, toolkit, obj_path, id=None):
        """An Foto instance of this deleted Flickr object.

        :param toolkit: As above for `SyncFollectionManager.sync()`.
        :param obj_path: As above for `SyncFollectionManager.sync()`.
        :param id: As above for `SyncFollectionManager.sync()`.
        """

        obj = self.find_or_model(toolkit.root_foll_dir, obj_path, id)
        if obj.id:
            res = toolkit.mrs_f.delete_foto(obj.id)
            if res:
                obj.delete()
        elif id:
                toolkit.mrs_f.delete_foto(id)
        return obj


    def capture_json(self, fson, fotoset=None, follection=None):
        """Captures Flick API's JSON response then saves and returns a
        `Foto` model instance.

        :param fson: The JSON object of a photo returned by the Flick API.
        :param fotoset: (Optional) A known photoset for this object.
        :param follection: (Optional) A known parent collection for this object.
        """
        obj = super().capture_json(fson, follection)

        obj.fotoset = fotoset
        obj.secret = self.flickr_attribute(fson, 'secret')
        obj.originalsecret = self.flickr_attribute(fson, 'originalsecret')
        obj.server = self.flickr_attribute(fson, 'server')
        obj.farm = self.flickr_attribute(fson, 'farm')
        obj.taken = self.flickr_attribute(fson, 'date_taken')
        obj.file_type = '.{}'.format(self.flickr_attribute(fson, 'originalformat'))
        obj.tags = self.flickr_attribute(fson, 'tags')

        if fotoset:
            file_name = obj.file_name.split("/")[-1]
            obj.path = self.get_obj_path(fotoset, obj.file_name)

        obj.path = obj.file_path_with_id
        obj.save()

        return obj


class FotoManager(FlickrObjectManager):
    """Standard objects manager for `Foto`."""
    pass


class Foto(FlickrObject):
    """Represents a synced system directory synced to Flickr as a Photoset."""

    id = models.CharField(primary_key=True, max_length=11, editable=True)
    fotoset = models.ForeignKey(
        'Fotoset',
        related_name="photos",
        related_query_name="photo",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    secret = models.CharField(max_length=12, blank=True, null=True,)
    originalsecret = models.CharField(max_length=12, blank=True, null=True,)
    server = models.CharField(max_length=12, blank=True, null=True,)
    farm = models.CharField(max_length=12, blank=True, null=True,)
    taken = models.DateField(auto_now=True, blank=True, null=True)
    file_type = models.CharField(max_length=6, blank=True, null=True,)
    tags = models.CharField(max_length=128, blank=True, null=True,)

    objects = FotoManager()
    file_manager = SyncFotoManager()

    class Meta:
        verbose_name = 'Photo'
        verbose_name_plural = 'Photos'

    @property
    def file_path_with_id(self):
        return os.path.join(
            os.path.split(self.path)[0],
            self.file_name_with_id
            )

    @property
    def file_name_with_id(self):
        # Only the first extension
        file_name_norm = self.file_name[
                    :self.file_name.index(self.file_type)+len(self.file_type)
                    ].split("/")[-1]
        return '{}.{}{}'.format(
                file_name_norm,
                self.id,
                self.file_type,
            )

    @property
    def file_name(self):
        return '{}{}'.format(self.title, self.file_type)

    def _path(self, secret):
        return 'https://farm{farm}.staticflickr.com/{server}/{foto}_{secret}'.format(
                                    farm=self.farm,
                                    server=self.server,
                                    foto=self.id,
                                    secret=secret,
                                    )

    def url(self):
        return '{}.jpg'.format(self._path(self.secret))

    def url75(self):
        return '{}_{}.jpg'.format(self._path(self.secret), 'sq')  # 75

    def url75D(self):
        return '{}_{}.jpg'.format(self._path(self.secret), 'd')  # 75

    def url100(self):
        return '{}_{}.jpg'.format(self._path(self.secret), 't')  # 100

    def url150(self):
        return '{}_{}.jpg'.format(self._path(self.secret), 'q')  # 150

    def url320(self):
        return '{}_{}.jpg'.format(self._path(self.secret), 'n')  # 320

    def url640(self):
        return '{}_{}.jpg'.format(self._path(self.secret), 'z')  # 640

    def url800(self):
        return '{}_{}.jpg'.format(self._path(self.secret), 'c')  # 640

    def urlLarge(self):
        return '{}_{}.jpg'.format(self._path(self.secret), 'b')  # 1024   k

    def urlOriginal(self):
        return '{}_{}.jpg'.format(self._path(self.originalsecret), 'o')


    def sync_to_fotoset(self, toolkit):

        fotoset_path = os.path.split(self.path)[0]
        target_fotoset = Fotoset.file_manager.model_by_path(
                                            toolkit.root_foll_dir,
                                            fotoset_path,
                                            )


        # Is there already a fotoset on Flickr for this?
        if not target_fotoset.id:

            # Add foto as primary to new fotoset. It will be uploaded
            self.fotoset = Fotoset.file_manager.sync(
                                            toolkit,
                                            fotoset_path,
                                            self,
                                            )

        else:

            # Add foto to the fotoset
            toolkit.mrs_f.add_foto_to_fotoset(
                                            self.id,
                                            target_fotoset.id
                                            )

            self.fotoset = target_fotoset

        self.follection = self.fotoset.follection
        self.save()
        return self.fotoset
