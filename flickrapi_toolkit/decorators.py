# -*- encoding: utf-8 -*-
"""Decorator which can be used to automatically authenticate to Flickr API
using a django View.
"""

import flickrapi
from django.conf import settings
from django.http import HttpResponseRedirect, HttpResponse

import logging
logging.basicConfig()

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


def require_flickr_auth(view):
    '''
    https://github.com/sybrenstuvel/flickrapi/blob/master/doc/3-auth.rst
    View decorator, redirects users to Flickr when no valid
    authentication token is available.
    '''

    def protected_view(request, *args, **kwargs):
        if 'token' in request.session:
            token = request.session['token']
            log.info('Getting token from session: %s' % token)
        else:
            token = None
            log.info('No token in session')

        f = flickrapi.FlickrAPI(
            settings.FLICKR_KEY,
            settings.FLICKR_SECRET,
            token=token,
            store_token=False
            )

        if token:
            # We have a token, but it might not be valid
            log.info('Verifying token')
            try:
                f.auth_checkToken()
            except flickrapi.FlickrError:
                token = None
                del request.session['token']

        if not token:
            # No valid token, so redirect to Flickr
            log.info('Redirecting user to Flickr to get frob')
            url = f.web_login_url(perms='read')
            return HttpResponseRedirect(url)

        # If the token is valid, we can call the decorated view.
        log.info('Token is valid')

        return view(request, *args, **kwargs)

    return protected_view


def callback(request):
    log.info('We got a callback from Flickr, store the token')

    f = flickrapi.FlickrAPI(
        settings.FLICKR_KEY,
        settings.FLICKR_SECRET,
        store_token=False
        )

    frob = request.GET['frob']
    token = f.get_token(frob)
    request.session['token'] = token

    return HttpResponseRedirect('project.home')
